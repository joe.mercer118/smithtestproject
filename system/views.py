from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse, HttpResponse
from functools import wraps
from django.utils.translation import gettext as trans
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from SmithProject.models import *
from django.core.exceptions import RequestDataTooBig, PermissionDenied
from io import BytesIO
from django.views.decorators.http import require_http_methods
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import csv


def _download_csv(queryset):
    opts = queryset.model._meta
    response = HttpResponse(content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="summary.csv"'
    writer = csv.writer(response)
    field_names = [field.name for field in opts.fields]
    writer.writerow(field_names)
    for obj in queryset:
        writer.writerow([getattr(obj, field) for field in field_names])
    return response


def stock_check(function):
    @wraps(function)
    def decorator(request, *args, **kwargs):
        required_product = int(request.POST.get('product_stock'))
        product_left = Product.objects.get(pk=request.POST.get('product_id')).stock_pcs
        if required_product <= product_left:
            return function(request, *args, **kwargs)
        else:
            return JsonResponse(status=540, data={"errorMessage": "over the product stock_pcs"})

    return decorator


def vip_check(function):
    @wraps(function)
    def decorator(request, *args, **kwargs):
        required_product = Product.objects.get(pk=request.POST.get('product_id'))
        if required_product.vip and not request.user.is_superuser:
            raise JsonResponse(status=541, data={"errorMessage": "Permission denied because you're not vip."})
        else:
            return function(request, *args, **kwargs)
    return decorator


def system_login(request):
    if str(request.user) != "AnonymousUser":
        return redirect('/dashboard/')
    if request.method == 'POST':
        user = authenticate(request, username=request.POST.get('email', None), password=request.POST.get('password', None))
        if user is not None:
            login(request, user)
            return JsonResponse(status=200, data={
                'result': True,
            })
        else:
            if AuthUser.objects.filter(username=request.POST.get("email", None)).count() != 0:
                return JsonResponse(status=400, data={
                    'errorMessage': trans('The password is incorrect, please check again')
                })
            return JsonResponse(status=400, data={
                'errorMessage': trans('Email account or password is incorrect.')
            })
    else:
        return render(
            request,
            'login.html',
            {}
        )


def system_logout(request):
    logout(request)
    return redirect("/login/")


@require_http_methods(["POST"])
def add_account(request):
    if AuthUser.objects.filter(username=request.POST.get("email", None)).count() != 0:
        return JsonResponse(status=400, data={
            'errorMessage': trans('The Email is already registered.')
        })
    User.objects.create_user(
        request.POST.get("email"),
        request.POST.get("email"),
        request.POST.get("password"),
        is_superuser=request.POST.get("isVip"),
    )
    user = authenticate(request, username=request.POST.get('email'),
                        password=request.POST.get('password'))
    login(request, user)
    return JsonResponse({
        'result': True,
    })


@login_required
def dashboard_index(request):
    all_products = Product.objects.all()
    if request.user.is_superuser:
        selectable_products = Product.objects.all()
    else:
        selectable_products = Product.objects.filter(vip=False).order_by('pk')
    all_orders = Order.objects.all()
    product_summary = serializers.serialize('json', all_products)
    return render(
        request,
        'dashboard.html',
        {
            "all_products": all_products,
            "all_orders": all_orders,
            "selectable_products": selectable_products,
            "product_summary": product_summary,
        }
    )


@require_http_methods(["POST"])
@login_required
@vip_check
@stock_check
@transaction.atomic
def add_product_to_court(request):
    required_product = Product.objects.get(pk=request.POST.get("product_id"))
    required_product_stock = int(request.POST.get("product_stock"))
    new_order = Order(
        qty=required_product_stock,
        product_id=required_product,
        user_id_id=request.user.id,
        shop_id=required_product.shop_id
    )
    required_product.shop_id.total_earn += int(request.POST.get("product_stock")) * required_product.price
    required_product.shop_id.total_order += 1
    required_product.shop_id.total_sold += int(request.POST.get("product_stock"))
    required_product.shop_id.save()
    new_order.save()
    required_product.stock_pcs -= required_product_stock
    required_product.sold_out += required_product_stock
    required_product.save()
    return JsonResponse(status=200, data={})


@require_http_methods(["POST"])
@login_required
@transaction.atomic
def remove_order(request):
    try:
        current_order = Order.objects.get(pk=request.POST.get("order_id"))
        current_order.shop_id.total_sold -= current_order.qty
        current_order.shop_id.total_earn -= current_order.qty * current_order.product_id.price
        current_order.shop_id.total_order -= 1
        current_order.shop_id.save()
        current_order.product_id.stock_pcs += current_order.qty
        current_order.product_id.save()
        current_order.delete()
        return JsonResponse(status=200, data={"message": "success"})
    except Exception as e:
        return JsonResponse(status=400, data={"errorMessage": "error occurred with {}".format(str(e))})


@require_http_methods(["POST"])
@login_required
def top_search(request):
    top_sold_outs = (Product.objects
                     .order_by('-sold_out')
                     .values_list('sold_out', flat=True)
                     .distinct())
    top3_products = (Product.objects
                     .order_by('-sold_out')
                     .filter(sold_out__in=top_sold_outs[:3]))[:3]
    return render_to_response(
        'topTable.html',
        {
            "top3_products": top3_products
        }
    )


@require_http_methods(["POST"])
@csrf_exempt
def product_arrival(request):
    try:
        arrived_product = Product.objects.get(pk=request.POST.get("product_id"))
        print(arrived_product.stock_pcs)
        arrived_product.stock_pcs += int(request.POST.get("product_stock"))
        arrived_product.save()
        return JsonResponse(status=200, data={"message": "success"})
    except Exception as e:
        return JsonResponse(status=400, data={"errorMessage": "error occurred with {}".format(str(e))})


@transaction.atomic
@require_http_methods(["POST"])
@csrf_exempt
def add_new_product(request):
    if Shop.objects.filter(name=request.POST.get("shop_id")).count() == 0:
        new_shop = Shop(
            name=request.POST.get("shop_id"),
            total_earn=0,
            total_sold=0,
            total_order=0
        )
        new_shop.save()
    try:
        new_product = Product(
            stock_pcs=request.POST.get("stock_pcs"),
            price=request.POST.get("price"),
            shop_id=Shop.objects.get(name=request.POST.get("shop_id")),
            vip=request.POST.get("vip"),
            sold_out=0
        )
        new_product.save()
        return JsonResponse(status=200, data={"message": "success"})
    except Exception as e:
        return JsonResponse(status=400, data={"errorMessage": "error occurred with {}".format(str(e))})


@login_required
def output_summary(request):
    data = _download_csv(Shop.objects.all())

    return HttpResponse(data, content_type='text/csv')


@login_required
def refresh(request):
    all_products = Product.objects.all()
    product_summary = serializers.serialize('json', all_products)
    return JsonResponse(status=200, data={"product_summary": product_summary})

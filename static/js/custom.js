 // SIDEBAR
 $(document).ready(function() {
 	$("#sidebar").mCustomScrollbar({
 		theme: "minimal"
 	});

 	$('#dismiss, .overlay').on('click', function() {
 		$('#sidebar').removeClass('active');
 		$('.overlay').removeClass('active');
 	});
 });

// DATATABLE
const mydata = [{
	license_key: "550e8400-e29b-41d4-a716-446655440000",
	license_keyType:"Standalone",
	user_id: "",
	validity_days: "350",
	validity_from: "",
	validity_to: "",
	user_name: "",
	user_phone: "",
	user_address: "",
	hardware_OS: "",
	hardware_CPU: "",
	hardware_RAM: "",
	hardware_MAC: ""
}, {
	license_key: "550e8400-e29b-41d4-a716-446655440000",
	license_keyType:"Rental",
	user_id: "bill_lee@tw.shuttle.com",
	validity_days: "90",
	validity_from: "2019-04-14",
	validity_to: "2023-07-01",
	user_name: "Shuttle",
	user_phone: "04-5452-8188",
	user_address: "台中市西屯路30號",
	hardware_OS: "Window 9",
	hardware_CPU: "intel i7 9th",
	hardware_RAM: "15.2gb",
	hardware_MAC: ["00:0C:29:01:98:28"]
}, {
	license_key: "550e8400-e29b-41d4-a716-446655440000",
	license_keyType:"Server",
	user_id: "Luios@tw.shuttle.com",
	validity_days: "700",
	validity_from: "2018-04-14",
	validity_to: "2018-04-14",
	user_name: "Shuttle",
	user_phone: "07-792-6188",
	user_address: "花蓮市瑞光路6巷3號",
	hardware_OS: "Window 10",
	hardware_CPU: "intel i7 9th",
	hardware_RAM: "15.2gb",
	hardware_MAC: ["00:0C:29:01:98:27"]
}, {
	license_key: "550e8400-e29b-41d4-a716-446655440000",
	license_keyType:"Standalone",
	user_id: "Bumnny@tw.shuttle.com",
	validity_days: "590",
	validity_from: "2016-04-14",
	validity_to: "2017-04-14",
	user_name: "Shuttle",
	user_phone: "06-792-6188",
	user_address: "台南市光路3號",
	hardware_OS: "Window 11",
	hardware_CPU: "intel i7 8th",
	hardware_RAM: "16.2gb",
	hardware_MAC: ["00:0C:29:01:55:27"]
}, {
	license_key: "330e8400-e29b-41d4-a716-4984898484",
	license_keyType:"Server",
	user_id: "Bumnny@tw.shuttle.com",
	validity_days: "910",
	validity_from: "2017-04-14",
	validity_to: "2019-04-14",
	user_name: "Shuttle",
	user_phone: "",
	user_address: "台南市光路3號",
	hardware_OS: "Window 11",
	hardware_CPU: "intel i7 8th",
	hardware_RAM: "16.2gb",
	hardware_MAC: ["00:0C:29:01:35:07","00:0C:29:01:35:08","00:0C:29:01:35:09"]
}, {
	order: "6",
	license_key: "550e8400-e29b-41d4-a716-446655440000",
	license_keyType:"Standalone",
	user_id: "",
	validity_days: "999",
	validity_from: "",
	validity_to: "",
	user_name: "",
	user_phone: "",
	user_address: "",
	hardware_OS: "",
	hardware_CPU: "",
	hardware_RAM: "",
	hardware_MAC: ""
}];

$(document).ready(function () {
	var table = $('#accountList').DataTable({
			"order": [[ 4, "desc" ]],
			"data": mydata,
			fixedHeader: {
				header: true
			},
			"dom": '<"toolbar">lfrtip',
			"columns": [{
					"data": "license_key"
				},
				{
					"data": "license_keyType"
				},
				{
					"data": "user_id"
				},
				{
					"data": "validity_days"
				},
				{   type: "date",
					"data": "validity_from"
				},
				{
					"data": "validity_to"
				},
				{
					"data": null,
					"render": function (data, type, full, meta) {
						return '<div class="btn-group" role="group"><button type="button" class="btn btn-sm btn-outline-success" data-html="true" data-trigger="click" data-toggle="popover" title="User Info" data-content="<div>Name/Company : ' + data['user_name'] + '<br>Phone : ' + data["user_phone"] + '<br>Address : ' + data["user_address"] + '</div>"><i class="far fa-id-card"></i></button><button type="button" class="btn btn-sm btn-outline-dark" data-html="true" data-trigger="click" data-toggle="popover" title="Hardware Info" data-content="<div>OS : ' + data['hardware_OS'] + '<br>CPU : ' + data["hardware_CPU"] + '<br>RAM : ' + data["hardware_RAM"] + '<br>MAC : ' + data['hardware_MAC'] + '</div>"><i class="fas fa-hdd"></i></button></div>'
					}
				},
				{
					"render": function (data, type, full, meta) {
						return '<div class="btn-group" role="group"><button class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#editAccountModal"><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#deleteAccountModal"><i class="fa fa-trash-alt"></i></button></div>'
					}
				}
			],
		});

	$("div.toolbar").html(
		'<h5>Account list<span><div class="btn-group btn-group-toggle ml-3" data-toggle="buttons"><label id="typeAll" class="btn btn-sm btn-outline-primary active"><input type="radio" name="options1" id="option1a" autocomplete="off" checked>All</label><label id="typeStandalone" class="btn btn-sm btn-outline-primary"><input type="radio" name="options1" id="option1b" autocomplete="off">Standalone</label><label id="typeRental" class="btn btn-sm btn-outline-primary"><input type="radio" name="options1" id="option1c" autocomplete="off">Rental</label><label id="typeServer" class="btn btn-sm btn-outline-primary"><input type="radio" name="options1" id="optiond" autocomplete="off">Server</label></div><div class="btn-group btn-group-toggle ml-3" data-toggle="buttons"><label id="statusAll" class="btn btn-sm btn-outline-warning active"><input type="radio" name="options2" id="option21" autocomplete="off" checked>All</label><label id="statusActive" class="btn btn-sm btn-outline-warning"><input type="radio" name="options2" id="option22" autocomplete="off">Active</label>	<label id="statusInactive" class="btn btn-sm btn-outline-warning"><input type="radio" name="options2" id="option22" autocomplete="off">Inactive</label></div><button class="btn btn-sm btn-outline-success ml-3" data-toggle="modal" data-target="#newAccountModal">New</button><span></h5>'
	);

	$('#typeAll').on( 'click', function () {
		table
			.search('')
			.columns(1)
			.search('')
			.draw();
	});	
	$('#typeStandalone, #typeRental, #typeServer').on( 'click', function () {
		table
			.columns(1)
			.search( $(this).text())
			.draw();
	});
	$('#statusAll').on( 'click', function () {
		table
			.search('')
			.columns(4)
			.search('')
			.draw();
	});
	$('#statusActive').on( 'click', function () {
		table
			.columns(4)
			.search('^[1-9]d*.d*|0.d*[1-9]d*$',true,false)
			.draw();
	});
	$('#statusInactive').on( 'click', function () {
		table
			.columns(4)
			.search('^$',true,false)
			.draw();
	});
	
});

$(function () {
	$('[data-toggle="popover"]').popover()
})

// TYPE SELECTION
$('#licenseKeyTypeSelect1').change(function () {
	if ($(this).val() === '1') {
		$('#collapseExpired1').collapse('show');
	}
	else {
		$('#collapseExpired1').collapse('hide');
	}
});

$('#licenseKeyTypeSelect2').change(function () {
	if ($(this).val() === '1') {
		$('#collapseExpired2').collapse('show');
	}
	else {
		$('#collapseExpired2').collapse('hide');
	}
});
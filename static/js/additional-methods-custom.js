$(function(){
    $.validator.addMethod( "validMac", function( value, element ) {
        return typeof(value)==='string' && value.length == 17 && value.split(':').length == 6;
    });

    $.validator.addMethod( "validGateway", function( value, element ) {
        var location_x = $('#location_x').val();
        var location_y = $('#location_y').val();
        return location_x !== undefined && location_x !== '' && location_y !== undefined && location_y !== '';
    });

    $.validator.addMethod( "validName", function( value, element ) {
        return typeof(value)==='string' && !(value.match(/[!%&'()*+./;<=>?\\,/:#@\t\r\n"\[\]\u007B-\u00BF-]/g));
    });
})

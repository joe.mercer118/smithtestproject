(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: JA (Japanese; 日本語)
 */
$.extend( $.validator.messages, {
	required: "このフィールドは必須です。",
	remote: "このフィールドを修正してください。",
	email: "有効なEメールアドレスを入力してください。",
	url: "有効なURLを入力してください。",
	date: "有効な日付を入力してください。",
	dateISO: "有効な日付（ISO）を入力してください。",
	number: "有効な数字を入力してください。",
	digits: "数字のみを入力してください。",
	creditcard: "有効なクレジットカード番号を入力してください。",
	equalTo: "パスワードが一致していません。",
	extension: "有効な拡張子を含む値を入力してください。",
	maxlength: $.validator.format( "{0} 文字以内で入力してください。" ),
	minlength: $.validator.format( "少なくとも {0} 文字以上である必要があります" ),
	rangelength: $.validator.format( "{0} 文字から {1} 文字までの値を入力してください。" ),
	range: $.validator.format( "{0} 〜 {1} の値を入力してください。" ),
	step: $.validator.format( "{0} の倍数を入力してください。" ),
	max: $.validator.format( "{0} 以下の値を入力してください。" ),
	min: $.validator.format( "{0} 以上の値を入力してください。" ),
	validMac: $.validator.format( "MACアドレスが違います" ),
	validGateway: $.validator.format( "ゲートウェイを追加し、正しい位置へ移動してください" ),
	validName: $.validator.format( "機器名の長さは2〜20文字です。「.」、「-」、および「_」以外のスペースまたは特殊文字は使用できません。" ),
	validPassword: $.validator.format( "Password field must not be blank or incompatible." ),
	validGroup: $.validator.format( "機器グループ名の長さは2〜20文字です。「.」、「-」、および「_」以外のスペースまたは特殊文字は使用できません。" ),
	validAccountName: $.validator.format( "ユーザー名の長さは2〜20文字です。「.」、「-」と「_」を除き、スペースまたは特殊文字は使用できません。" ),
	validAccountGroup: $.validator.format( "アカウントグループ名の長さは2〜20文字です。「.」、「-」、および「_」以外のスペースまたは特殊文字は使用できません。" ),
	validEmail: "有効なEメールアドレスを入力してください。",
	validAccount: "電子メール アカウントが既に割り当てられています",
	validDeviceGroup: "機器グループ名は既に存在します。",
	validAccountNameExist: "ユーザー名既に存在します。",
	validDeviceNameExist: "機器名は既に存在します。",
} );
return $;
}));
(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: RU (Russian; русский язык)
 */
$.extend( $.validator.messages, {
	required: "Это поле необходимо заполнить.",
	remote: "Пожалуйста, введите правильное значение.",
	email: "Пожалуйста, введите корректный адрес электронной почты.",
	url: "Пожалуйста, введите корректный URL.",
	date: "Пожалуйста, введите корректную дату.",
	dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
	number: "Пожалуйста, введите число.",
	digits: "Пожалуйста, вводите только цифры.",
	creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
	equalTo: "Пароль и пароль подтверждения не совпадают.",
	extension: "Пожалуйста, выберите файл с правильным расширением.",
	maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
	minlength: $.validator.format( "Должно быть не менее {0} символов." ),
	rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
	range: $.validator.format( "Пожалуйста, введите значение от {0} до {1}." ),
	max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
	min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." ),
	validMac: $.validator.format( "Неверный MAC-адрес" ),
	validGateway: $.validator.format( "Пожалуйста, добавьте шлюз и переместите его в правильное положение" ),
	validName: $.validator.format( "Длина имени устройства составляет от 2 до 20 символов, пробелы или специальные символы не допускаются, кроме «.», «-» и «_»." ),
	validPassword: $.validator.format( "Password field must not be blank or incompatible." ),
	validGroup: $.validator.format( "Длина имени группы устройств составляет от 2 до 20 символов, пробелы или специальные символы не допускаются, кроме «.»,  «-» и «_»." ),
	validAccountName: $.validator.format( "Длина имени пользователя составляет 2 ~ 20 символов, пробелы или специальные символы не допускаются, кроме«.», «-» и «_»." ),
	validAccountGroup: $.validator.format( "Длина наименования группы аккаунта составляет 2 ~ 20 символов, пробелы или специальные символы не допускаются, кроме «.», «-» и «_»." ),
	validEmail: "Пожалуйста, введите корректный адрес электронной почты.",
	validAccount: "Учетная запись электронной почты уже назначена.",
	validDeviceGroup: "Имя группы устройств уже существует.",
	validAccountNameExist: "Имя пользователя уже существует.",
	validDeviceNameExist: "Имя устройства уже существует.",
} );
return $;
}));
(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ES (Spanish; Español)
 */
$.extend( $.validator.messages, {
	required: "Este campo es obligatorio.",
	remote: "Por favor, rellena este campo.",
	email: "Por favor, escribe una dirección de correo válida.",
	url: "Por favor, escribe una URL válida.",
	date: "Por favor, escribe una fecha válida.",
	dateISO: "Por favor, escribe una fecha (ISO) válida.",
	number: "Por favor, escribe un número válido.",
	digits: "Por favor, escribe sólo dígitos.",
	creditcard: "Por favor, escribe un número de tarjeta válido.",
	equalTo: "Contraseña y contraseña confirmada no coinciden.",
	extension: "Por favor, escribe un valor con una extensión aceptada.",
	maxlength: $.validator.format( "Por favor, no escribas más de {0} caracteres." ),
	minlength: $.validator.format( "Debe tener al menos {0} caracteres de largo." ),
	rangelength: $.validator.format( "Por favor, escribe un valor entre {0} y {1} caracteres." ),
	range: $.validator.format( "Ingrese un valor del {0} al {1}." ),
	max: $.validator.format( "Por favor, escribe un valor menor o igual a {0}." ),
	min: $.validator.format( "Por favor, escribe un valor mayor o igual a {0}." ),
	nifES: "Por favor, escribe un NIF válido.",
	nieES: "Por favor, escribe un NIE válido.",
	cifES: "Por favor, escribe un CIF válido.",
	validMac: $.validator.format( "Dirección MAC es válida" ),
	validGateway: $.validator.format( "Por favor agregue un Gateway y muévalo a la posición correcta" ),
	validName: $.validator.format( 'La longitud del nombre del dispositivo es de 2 ~ 20 caracteres, no se permiten espacios ni caracteres especiales, excepto ".", "-" y "_".' ),
	validPassword: $.validator.format( "Password field must not be blank or incompatible." ),
	validGroup: $.validator.format( 'La longitud del nombre del grupo de dispositivos es de 2 ~ 20 caracteres, no se permiten espacios ni caracteres especiales, excepto ".", "-" y "_".' ),
	validAccountName: $.validator.format( 'La longitud del nombre de usuario es de 2 ~ 20 caracteres, no se permiten espacios ni caracteres especiales, excepto ".", "-" y "_".' ),
	validAccountGroup: $.validator.format( 'La longitud del nombre del grupo de cuentas es de 2 ~ 20 caracteres, no se permiten espacios ni caracteres especiales, excepto ".", "-" y "_".' ),
	validEmail: "Por favor, escribe una dirección de correo válida.",
	validAccount: "La cuenta de correo electrónico ya está asignada.",
	validDeviceGroup: "El nombre del grupo de dispositivos ya existe.",
	validAccountNameExist: "El nombre de usuario ya existe.",
	validDeviceNameExist: "El nombre del dispositivo ya existe.",
} );
return $;
}));
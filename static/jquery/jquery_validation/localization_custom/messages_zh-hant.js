(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ZH (Chinese; 中文 (Zhōngwén), 汉语, 漢語)
 * Region: TW (Taiwan)
 */
$.extend( $.validator.messages, {
	required: "必須填寫",
	remote: "請修正此欄位",
	email: "請輸入有效的電子郵件",
	url: "請輸入有效的網址",
	date: "請輸入有效的日期",
	dateISO: "請輸入有效的日期 (YYYY-MM-DD)",
	number: "請輸入正確的數值",
	digits: "只可輸入數字",
	creditcard: "請輸入有效的信用卡號碼",
	equalTo: "密碼和確認密碼不相符。",
	extension: "請輸入有效的後綴",
	maxlength: $.validator.format( "最多 {0} 個字" ),
	minlength: $.validator.format( "至少 {0} 個字元" ),
	rangelength: $.validator.format( "請輸入長度為 {0} 至 {1} 之間的字串" ),
	range: $.validator.format( "請輸入一個介於 {0} 到 {1} 之間的值。" ),
	max: $.validator.format( "請輸入不大於 {0} 的數值" ),
	min: $.validator.format( "請輸入不小於 {0} 的數值" ),
	validMac: $.validator.format( "無效的MAC位址" ),
	validGateway: $.validator.format( "請新增定位器，並將其移動到正確的位置" ),
	validName: $.validator.format( '設備名稱長度為2〜20個字元，不允許空格或特殊字元，除了「.」、「-」及「_」。' ),
	validPassword: $.validator.format( "密碼欄不得為空白或格式不符" ),
	validGroup: $.validator.format( '設備群組名稱長度為2〜20個字元，不允許空格或特殊字元，除了「.」、「-」及「_」。' ),
	validAccountName: $.validator.format( '使用者名稱長度為2~20個字元，不允許空格或特殊字元，除了「.」、「-」及「_」。' ),
	validAccountGroup: $.validator.format( '帳號群組名稱長度為2〜20個字元，不允許空格或特殊字元，除了「.」、「-」及「_」。' ),
	validEmail: "請輸入有效的電子郵件",
	validAccount: "此電子郵件帳號被使用。",
	validDeviceGroup: "設備群組名稱已存在。",
	validAccountNameExist: "使用者名稱已存在。",
	validDeviceNameExist: "設備名稱已存在。",
} );
return $;
}));
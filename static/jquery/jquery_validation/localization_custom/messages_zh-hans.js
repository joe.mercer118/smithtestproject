(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ZH (Chinese, 中文 (Zhōngwén), 汉语, 漢語)
 */
$.extend( $.validator.messages, {
	required: "这是必填字段",
	remote: "请修正此字段",
	email: "请输入有效的电子邮件地址",
	url: "请输入有效的网址",
	date: "请输入有效的日期",
	dateISO: "请输入有效的日期 (YYYY-MM-DD)",
	number: "请输入有效的数字",
	digits: "只能输入数字",
	creditcard: "请输入有效的信用卡号码",
	equalTo: "密码和确认密码不匹配。",
	extension: "请输入有效的后缀",
	maxlength: $.validator.format( "最多可以输入 {0} 个字符" ),
	minlength: $.validator.format( "至少 {0} 个字符" ),
	rangelength: $.validator.format( "请输入长度在 {0} 到 {1} 之间的字符串" ),
	range: $.validator.format( "请输入一个介于 {0} 到 {1} 之间的值。" ),
	step: $.validator.format( "请输入 {0} 的整数倍值" ),
	max: $.validator.format( "请输入不大于 {0} 的数值" ),
	min: $.validator.format( "请输入不小于 {0} 的数值" ),
	validMac: $.validator.format( "无效的MAC 地址" ),
	validGateway: $.validator.format( "请添加网关，并将其移动到正确的位置" ),
	validName: $.validator.format( "设备名称长度为2〜20个字符，除「.」、「-」及「_」外，不允许使用空格或特殊字符。" ),
	validPassword: $.validator.format( "Password field must not be blank or incompatible." ),
	validGroup: $.validator.format( "设备组名称长度为2〜20个字符，除「.」、「-」及「_」外，不允许使用空格或特殊字符。" ),
	validAccountName: $.validator.format( '用户名长度为2〜20个字符，除「.」、「-」及「_」外，不允许使用空格或特殊字符。' ),
	validAccountGroup: $.validator.format( "帐户组名称长度为2〜20个字符，除「.」、「-」及「_」外，不允许使用空格或特殊字符。" ),
	validEmail: "请输入有效的电子邮件地址",
	validAccount: "电子邮件帐户已分配。",
	validDeviceGroup: "设备组名称已存在。",
	validAccountNameExist: "用户名已存在。",
	validDeviceNameExist: "设备名称已存在。",
} );
return $;
}));
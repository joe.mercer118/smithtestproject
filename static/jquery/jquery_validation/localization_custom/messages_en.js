(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: ZH (Chinese; 中文 (Zhōngwén), 汉语, 漢語)
 * Region: TW (Taiwan)
 */
$.extend( $.validator.messages, {
	required: "This field is required.",
	remote: "Please fix this field.",
	email: "The email address format is incorrect!",
	url: "Please enter a valid URL.",
	date: "Please enter a valid date.",
	dateISO: "Please enter a valid date (ISO).",
	number: "Please enter a valid number.",
	digits: "Please enter only digits.",
	equalTo: "Password and confirm password do not match.",
	maxlength: $.validator.format( "Please enter no more than {0} characters." ),
	minlength: $.validator.format( "It must be at least {0} characters long." ),
	rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
	range: $.validator.format( "Please enter a value from {0} to {1}." ),
	max: $.validator.format( "Please enter a value less than or equal to {0}." ),
	min: $.validator.format( "Please enter a value greater than or equal to {0}." ),
	step: $.validator.format( "Please enter a multiple of {0}." ),
	validMac: $.validator.format( "Invalid MAC address" ),
	validGateway: $.validator.format( "Please add a gateway and move it to the right position" ),
	validName: $.validator.format( 'The device name length is 2~20 characters, spaces or special characters are not allowed, except ".", "-" and "_".' ),
	validPassword: $.validator.format( "Password field must not be blank or incompatible." ),
	validGroup: $.validator.format( 'The device group name length is 2~20 characters,  spaces or special characters are not allowed,  except ".", "-" and "_".' ),
	validAccountName: $.validator.format( 'The user name length is 2~20 characters,  spaces or special characters are not allowed,  except "." ,"-"and "_".' ),
	validAccountGroup: $.validator.format( 'The  account group name length is 2~20 characters,  spaces or special characters are not allowed,  except ".", "-" and "_".' ),
	validEmail: "The email address format is incorrect!",
	validAccount: "Email account is already assigned.",
	validDeviceGroup: "The device group name already exists.",
	validAccountNameExist: "The user name already exists.",
	validDeviceNameExist: "The device name already exists.",
} );
return $;
}));
$(function(){
    $.validator.addMethod( "validMac", function( value, element ) {
        return typeof(value)==='string' && value.length == 17 && value.split(':').length == 6;
    });

    $.validator.addMethod( "validGateway", function( value, element ) {
        var location_x = $('#location_x').val();
        var location_y = $('#location_y').val();
        return location_x !== undefined && location_x !== '' && location_y !== undefined && location_y !== '';
    });

    $.validator.addMethod( "validName", function( value, element ) {
        return typeof(value)==='string' && !(value.match(/[!%&`$^'()*+/;<=>?\\,/:#@\t\r\n"\[\]\s\u007B-\u00BF]/g));
    });

    $.validator.addMethod( "validPassword", function( value, element ) {
        return typeof(value)==='string' && !(value.match(/[\s\u007B-\u00BF-]/g));
    });

    $.validator.addMethod( "validGroup", function( value, element ) {
        return typeof(value)==='string' && !(value.match(/[!%&`$^'()*+/;<=>?\\,/:#@\t\r\n"\[\]\s\u007B-\u00BF]/g));
    });

    $.validator.addMethod( "validAccountName", function( value, element ) {
        return typeof(value)==='string' && !(value.match(/[!%&`$^'()*+/;<=>?\\,/:#@\t\r\n"\[\]\s\u007B-\u00BF]/g));
    });

    $.validator.addMethod( "validAccountGroup", function( value, element ) {
        return typeof(value)==='string' && !(value.match(/[!%&`$^'()*+/;<=>?\\,/:#@\t\r\n"\[\]\s\u007B-\u00BF]/g));
    });

    $.validator.addMethod( "validEmail", function( value, element ) {
        var value_split = value.split('@');
        if( typeof(value)==='string' && value_split.length==2){
            return value_split[0].length<=64 && value_split[1].length<=255;
        }else{
            return false;
        }
    });

    $.validator.addMethod( "validAccount", function( value, element ) {
        var status = false;
        $.ajax({
            url : '/account/check/',
            method : 'POST',
            async : false,
            data : {
                email : value
            },
            error : function(e){
                console.log(e);
            },
            success : function(data){
                status = data.result;
            }
        })
        return status;
    });

    $.validator.addMethod( "validDeviceGroup", function( value, element ) {
        var status = false;
        var id = $(element).attr('data-id');
        $.ajax({
            url : '/device/group/check/',
            method : 'POST',
            async : false,
            data : {
                name : value,
                id : id,
            },
            error : function(e){
                console.log(e);
            },
            success : function(data){
                status = data.result;
            }
        })
        return status;
    });

    $.validator.addMethod( "validAccountNameExist", function( value, element ) {
        var status = false;
        var id = $(element).attr('data-id');
        $.ajax({
            url : '/account/name/check/',
            method : 'POST',
            async : false,
            data : {
                name : value,
                id : id,
            },
            error : function(e){
                console.log(e);
            },
            success : function(data){
                status = data.result;
            }
        })
        return status;
    });

    $.validator.addMethod( "validDeviceNameExist", function( value, element ) {
        var status = false;
        var id = $(element).attr('data-id');
        $.ajax({
            url : '/device/name/check/',
            method : 'POST',
            async : false,
            data : {
                name : value,
                id : id,
            },
            error : function(e){
                console.log(e);
            },
            success : function(data){
                status = data.result;
            }
        })
        return status;
    });
})
